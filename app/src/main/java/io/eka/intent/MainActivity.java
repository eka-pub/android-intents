package io.eka.intent;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import com.google.gson.Gson;

import java.util.HashMap;

public class MainActivity extends AppCompatActivity {

    TextView mStatus,json;
    Gson gson = new Gson();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        findViewById(R.id.start).setOnClickListener(view -> {
            EkaIntentUtils.startMileageTracking(this);
        });
        findViewById(R.id.end).setOnClickListener(view -> {
            EkaIntentUtils.stopMileageTracking(this);
        });
        findViewById(R.id.checkin).setOnClickListener(view -> {
            EkaIntentUtils.checkInMileageTracking("Deviation Point",this);
        });
        mStatus = findViewById(R.id.status);
        json = findViewById(R.id.json);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        EkaIntentUtils.Status status = EkaIntentUtils.checkStatus(data,requestCode,resultCode);
        mStatus.setText(status.name());
        try {
            HashMap<String,String> map = EkaIntentUtils.fetchData(data,requestCode);
            json.setText(gson.toJson(map));
        }catch (Exception e){

        }
    }
}
