package io.eka.intent;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;

import com.google.gson.Gson;

import java.util.HashMap;

public class EkaIntentUtils {


    public final static int START_MILEAGE_TRACKING = 7001;
    public final static int STOP_MILEAGE_TRACKING = 7002;
    public final static int CHECKIN_MILEAGE_TRACKING = 7003;

    /**
     * @param context
     * @code 7001 for start mileage tracking
     */
    public static void startMileageTracking(Activity context){
        Intent mainIntent = new Intent();
        mainIntent.setAction(Intent.ACTION_VIEW);
        mainIntent.putExtra("caller",context.getClass().getName());
        mainIntent.setData(Uri.parse("dice://dice/intent/miles/start"));
        context.startActivityForResult(mainIntent,START_MILEAGE_TRACKING);
    }

    /**
     * @param context
     * @code 7002 for stop mileage tracking
     */
    public static void stopMileageTracking(Activity context){
        Intent mainIntent = new Intent();
        mainIntent.setAction(Intent.ACTION_VIEW);
        mainIntent.putExtra("caller",context.getClass().getName());
        mainIntent.setData(Uri.parse("dice://dice/intent/miles/stop"));
        context.startActivityForResult(mainIntent,STOP_MILEAGE_TRACKING);
    }

    /**
     *
     * @param type :- Created dynamically from dice dashboard
     * @param context :- calling activity context passed
     * @param json :- Any data u want us to handle and post to our servers
     */
    public static void checkInMileageTracking(String type,Activity context,String json){
        Intent mainIntent = new Intent();
        mainIntent.setAction(Intent.ACTION_VIEW);
        mainIntent.putExtra("attrs",json);
        mainIntent.putExtra("caller",context.getClass().getName());
        mainIntent.setData(Uri.parse(String.format("dice://dice/intent/checkin/%s",type)));
        context.startActivityForResult(mainIntent,CHECKIN_MILEAGE_TRACKING);
    }

    /**
     * @param type :- Created dynamically from dice dashboard
     * @param context :- calling activity context passed
     */
    public static void checkInMileageTracking(String type,Activity context){
        Intent mainIntent = new Intent();
        mainIntent.setAction(Intent.ACTION_VIEW);
        mainIntent.putExtra("attrs","{}");
        mainIntent.putExtra("caller",context.getClass().getName());
        mainIntent.setData(Uri.parse(String.format("dice://dice/intent/checkin/%s",type)));
        context.startActivityForResult(mainIntent,CHECKIN_MILEAGE_TRACKING);
    }

    /**
     *
     * @param intent
     * @param requestCode
     * @return
     *  - START_MILEAGE_TRACKING
     *    { "type":"twoWheeler","token":"tripId" }
     *  - STOP_MILEAGE_TRACKING
     *    { "type":"twoWheeler","distance","","token":"tripId","transId":"transId" }
     *  - CHECKIN_MILEAGE_TRACKING
     *    { "id":"Submission id" }
     */
    public static HashMap<String,String> fetchData(Intent intent, int requestCode){
        HashMap<String,String> map = new HashMap<>();
        switch (requestCode){
            case START_MILEAGE_TRACKING:{
                map.put("type",intent.getStringExtra("type"));
                map.put("token",intent.getStringExtra("token"));
            }
            break;
            case STOP_MILEAGE_TRACKING:{
                map.put("transId",intent.getStringExtra("transId"));
                map.put("type",intent.getStringExtra("type"));
                map.put("distance",intent.getStringExtra("distance"));
                map.put("token",intent.getStringExtra("token"));
            }
            break;
            case CHECKIN_MILEAGE_TRACKING:{
                map.put("status",intent.getStringExtra("status"));
                map.put("request",intent.getStringExtra("request"));
                map.put("distance",intent.getStringExtra("distance"));
                map.put("reference",intent.getStringExtra("reference"));
                map.put("type",intent.getStringExtra("type"));
            }
            break;
        }
        return map;
    }


    /**
     * Pass parameters directly from activity result
     * @param intent
     * @param requestCode
     * @param resultCode
     * @return Status
     */
    public static Status checkStatus(Intent intent,int requestCode,int resultCode){
        if(requestCode!=7001 && requestCode!=7002 &&  requestCode!=7003)
            return Status.UNKNOWN;
        if(resultCode==0)
            return Status.CANCELLED;
        else if(resultCode==-1){
            return Status.SUCCESS;
        }else if(resultCode==-2){
            return Status.DISCARDED;
        }
        return Status.UNKNOWN;
    }

    public static enum Status{
        SUCCESS,
        UNKNOWN,
        DISCARDED,
        CANCELLED
    }

}
